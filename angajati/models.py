import datetime
from datetime import date, timedelta, datetime
#!!! fara importul de mai sus imi da eroare la pontaj lunar, cu date time... am lasat asa, chiar daca e "nefolosit"
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import gettext_lazy as _


from django.contrib.auth.models import AbstractUser, PermissionsMixin, User
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.db import models
from django.utils import timezone
from django_iban.fields import IBANField
from angajati.validators import validare_cnp


class ModelMixin:
    data_introducere = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{str(self.data_introducere)}"


class Employee(ModelMixin, models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    cnp = models.CharField(max_length=13, validators=[MinLengthValidator(13), validare_cnp, MaxLengthValidator(13)],
                           null=True, blank=True)
    nume_angajat = models.CharField(max_length=60)
    prenume_angajat = models.CharField(max_length=60)
    id_pontaj = models.BigIntegerField(unique=True, null=True, blank=True)
    telefon = models.CharField(max_length=18, default="0", null=True, blank=True)
    email = models.EmailField(default="ceva@ceva.com", null=True, blank=True)
    data_nasterii = models.DateField(default=timezone.now, null=True, blank=True)
    religie = models.CharField(max_length=30, default="religie nedefinita", null=True, blank=True)
    data_angajare = models.DateField(default=timezone.now, null=True, blank=True)
    data_introducere = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.nume_angajat.capitalize()} {self.prenume_angajat.capitalize()} ' \
               f'Id pontaj:{self.id_pontaj}'

    def save(self, *args, **kwargs):
        try:
            status_set = Status.objects.get(abbreviated_value='ANG')
            super(Employee, self).save(*args, **kwargs)
            StatusAngajat.objects.create(angajat_id=self.id, status=status_set)

        except ObjectDoesNotExist:
            Status.objects.create(view_value="ANGAJAT", abbreviated_value='ANG')
            status_set = Status.objects.get(abbreviated_value='ANG')
            super(Employee, self).save(*args, **kwargs)
            StatusAngajat.objects.create(angajat=self, status=status_set)

# TODO pop up non-blocant daca are sub 18 ani!

class Comment(ModelMixin, models.Model):
    angajat = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True, blank=True)
    continut = models.TextField()
    data_introducere = models.DateTimeField(auto_now=True, )
    nume_autor = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    # in settings am modificat TIME_ZONE = 'Europe/Bucharest'

    def __str__(self):
        return f"{self.continut[:15]} \n |posted by:{self.nume_autor} on " \
               f"{self.data_introducere.strftime('%Y-%m-%d ora: %H:%M')}"


class Status(ModelMixin, models.Model):
    view_value = models.CharField(max_length=100)
    abbreviated_value = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.abbreviated_value}:{self.view_value}"


# TODO vreau sa pot selecta un interval de mai multe zile, de exemplu pentru Boala/ Concediu
class StatusAngajat(ModelMixin, models.Model):
    angajat = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True, blank=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True, blank=True)
    date_time_in = models.DateTimeField(default=timezone.now, null=True, blank=True)
    date_time_out = models.DateTimeField(default=timezone.now, null=True, blank=True)
    note = models.TextField(default=None, null=True, blank=True)

    def hours_worked(self):
        ore_lucrate = self.date_time_out - self.date_time_in
        if str(ore_lucrate) == "0:00:00":
            ore_lucrate = 0
        return ore_lucrate

    def __str__(self):
        return f"{self.angajat}:{self.status}//in:{self.date_time_in}//out:{self.date_time_out}"


    # https://django-durationfield.readthedocs.io/en/latest/
    # de implementat logica pentru diferite status-uri
    # ore_lucrate = datetime.combine(date.min, self.last_out) - datetime.combine(date.min, self.first_in)


class Functie(ModelMixin, models.Model):
    view_value = models.CharField(max_length=100)

    def __str__(self):
        return self.view_value


class ContractDetail(ModelMixin, models.Model):
    angajat = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True, blank=True)
    functie = models.ForeignKey(Functie, on_delete=models.CASCADE)
    salar_brut = models.DecimalField(max_digits=10, decimal_places=2)
    naveta = models.CharField(max_length=50, default="URBAN",
                              choices=[("URBAN", "URBAN"), ("RURAL", "RURAL"), ("ALTA SITUATIE", "ALTA SITUATIE")])
    data_inceperii = models.DateField(default=timezone.now)
    data_incetarii = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"{self.functie}, {self.salar_brut} brut, data:{self.data_inceperii};{self.naveta}"

# TODO implementeaza si pentru django admin limitare la 1 singura companie
class Company(models.Model):
    nume = models.CharField(max_length=250)
    cui = models.CharField(max_length=15, default="RO........", null=True, blank=True)
    cif = models.CharField(max_length=15, default="J./.../....", null=True, blank=True)
    adresa = models.TextField(null=True, blank=True)
    email = models.EmailField(default="undefined@undefined.com", null=True, blank=True)
    website = models.URLField('Website', max_length=200, null=True, blank=True)
    telefon = models.CharField(max_length=100)
    cont_bancar_ron = IBANField(enforce_database_constraint=True, unique=True, null=True, blank=True)
    cont_bancar_eur = IBANField(enforce_database_constraint=True, unique=True, null=True, blank=True)
    data_infiintarii = models.DateField(null=True, blank=True)
    cod_caen = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return f"{self.nume}"

# functie = models.CharField(max_length=80, default="OPERATOR",
#    choices=[("OPERATOR", "OPERATOR"), ("OPERATOR CNC", "OPERATOR CNC"),
#          ("VOPSITOR", "VOPSITOR"), ("TEHNICIAN CALITATE", "TEHNICIAN CALITATE"),
#          ("LACATUS MECANIC", "LACATUS MECANIC"), ("MAGAZINER", "MAGAZINER"),
#          ("SUDOR", "SUDOR")],
#                            null=True, blank=True)

# class Pontaj(models.Model):
#     date = models.DateField(null=True, blank=True)
#     free_day = models.BooleanField(default=False)
#     weekend = models.BooleanField(default=False)

# _in = models.DateTimeField(null=True, blank=True)
# _out = models.DateTimeField(null=True, blank=True)

# class OreLucrate(models.Model):
#     nume = models.CharField(max_length=250)
#     prenume = models.CharField(max_length=250)
#     nr_cartela = models.CharField(max_length=10)
#     ziua = models.DateTimeField(default=timezone.now, null=True, blank=True)
#     zile_lucratoare =
#     ore_lucratoare =
#     status_zi = models.CharField(max_length=100)
#
#     data_introducere = models.DateTimeField(auto_now=True)


# class User(PermissionsMixin, AbstractUser):
#     parent_angajat = models.ForeignKey(Employee, on_delete=models.CASCADE)
# voi folosi model-ul user din django
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html
# https://stackoverflow.com/questions/67560240/how-to-link-an-existing-model-with-user-model-in-django
# class User(models.Model):
#     nume_user = models.CharField(max_length=15)
#     data_creare = models.DateTimeField(auto_now=True)
#
#     def __str__(self):
#         return self.nume_user


# TODO user profiles
# class UserProfile(models.Model):
#     TYPE_CHOICES = (
#         ('administrative', 'administrative'),
#         ('birouri', 'birouri'),
#         ('employee', 'employee')
#         )
