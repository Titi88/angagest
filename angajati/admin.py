from django.contrib import admin
from django.contrib.auth.models import Permission
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from angajati.models import *


# Register your models here.
admin.site.register(Permission)
admin.site.register(Employee)
admin.site.register(Comment)
# admin.site.register(User)
admin.site.register(Status)
admin.site.register(StatusAngajat)
admin.site.register(Functie)
admin.site.register(ContractDetail)
admin.site.register(Company)
# admin.site.register(Pontaj)


class EmployeeResource(resources.ModelResource):
    class Meta:
        model = Employee

class StatusResource(resources.ModelResource):
    class Meta:
        model = Status


class StatusAngajatResource(resources.ModelResource):
    class Meta:
        model = StatusAngajat
        # fields = ('angajat', 'date', 'first_in', 'last_out')


# @admin.register(Employee)
# class EmployeeAdmin(ImportExportModelAdmin):
#     pass