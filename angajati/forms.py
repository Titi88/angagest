from django import forms

from angajati.models import *


class AngajatForm(forms.ModelForm):
    class Meta:
        # in interiorul class Meta se pun specificatiile modelului si field-urilor formularului
        model = Employee
        fields = '__all__'  # o lista cu toate field-urilor modelului
        exclude = ['user']
        # fields = [' ', ' ', ' ']  # o lista doar field-urile necesare din model
        # sau  exclude = [' ', ' ', ' ']


class ContractDetailUpdateForm(forms.ModelForm):
    class Meta:
        model = ContractDetail
        fields = '__all__'
        exclude = ['angajat']

class ContractDetailCreateForm(forms.ModelForm):
    class Meta:
        model = ContractDetail
        fields = '__all__'
        exclude = ['angajat']


class StatusAngajatUpdateForm(forms.ModelForm):
    class Meta:
        model = StatusAngajat
        fields = ['status', 'date_time_in','date_time_out', 'note']


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = '__all__'


class FunctieForm(forms.ModelForm):
    class Meta:
        model = Functie
        fields = '__all__'


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = '__all__'


class CommentFormAngajat(forms.ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'
        exclude = ['nume_autor', 'angajat']


# class ContractDetailForm(forms.ModelForm):
#     class Meta:
#         model = ContractDetail
#         fields = '__all__'
#
# class StatusAngajatForm(forms.ModelForm):
#     class Meta:
#         model = StatusAngajat
#         fields = ['angajat', 'status', 'date']

# am facut formularul direct in templates, in hompage, cu 2 input type hidden si buton de Reactiveaza user
# class UserActivationForm(forms.Form):
#     user = forms.Select()
#     activate = forms.CheckboxInput()
