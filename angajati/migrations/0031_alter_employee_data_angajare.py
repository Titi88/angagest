# Generated by Django 3.2.7 on 2022-01-26 11:58

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('angajati', '0030_alter_comment_angajat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='data_angajare',
            field=models.DateField(blank=True, default=datetime.date(2022, 1, 26), null=True),
        ),
    ]
