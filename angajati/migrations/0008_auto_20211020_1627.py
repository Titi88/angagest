# Generated by Django 3.2.7 on 2021-10-20 13:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('angajati', '0007_alter_statusangajat_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='abbreviated_value',
            field=models.CharField(default='ANG', max_length=100),
        ),
        migrations.AlterField(
            model_name='status',
            name='view_value',
            field=models.CharField(default='ANGAJAT', max_length=100),
        ),
        migrations.AlterField(
            model_name='statusangajat',
            name='status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='angajati.status'),
        ),
    ]
