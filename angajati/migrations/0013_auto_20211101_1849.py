# Generated by Django 3.2.7 on 2021-11-01 16:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('angajati', '0012_employee_id_pontaj'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contractdetail',
            name='angajat',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='angajati.employee'),
        ),
        migrations.AlterField(
            model_name='statusangajat',
            name='angajat',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='angajati.employee'),
        ),
    ]
