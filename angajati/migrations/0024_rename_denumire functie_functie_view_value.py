# Generated by Django 3.2.7 on 2022-01-12 16:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('angajati', '0023_rename_view_value_functie_denumire functie'),
    ]

    operations = [
        migrations.RenameField(
            model_name='functie',
            old_name='denumire functie',
            new_name='view_value',
        ),
    ]
