# Generated by Django 3.2.7 on 2022-01-12 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('angajati', '0021_alter_employee_telefon'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='id_pontaj',
            field=models.BigIntegerField(blank=True, null=True, unique=True),
        ),
    ]
