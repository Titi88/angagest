from django.urls import path

from angajati import views

urlpatterns = [
    path('', views.home_page_view, name='home'),

    path('creaza_angajat/', views.create_angajat_view, name='creaza_angajat'),
    path('upload_angajati/', views.upload_angajati_view, name='upload_angajati'),
    path('employee_details/<int:angajat_id>/', views.employee_details_view, name='details_employee'),
    path('employees_actives/', views.employees_actives_view, name='employees_actives'),
    path('employee_all_view/', views.employee_all_view, name='employee_all_view'),
    path('update_angajat/<int:pk>/', views.EmployeeUpdateView.as_view(), name='update_angajat'),
    path('delete_angajat/<int:pk>/', views.disable_employee_view, name='delete_angajat'),
    path('export_csv_angajati', views.export_csv_angajati, name='export_csv_angajati'),
    path('download_file_for_upload_angajati/', views.download_file_for_upload_angajati,
         name='download_file_for_upload_angajati'),

    path('upload_csv/', views.upload_pontaj_csv_view, name='upload_csv'),
    path('pontaj_lunar/', views.pontaj_lunar_view, name='pontaj_lunar'),
    path('pontaj_simplificat/', views.pontaj_simplificat_view, name='pontaj_simplificat'),
    path('pontaj_lunar_angajat/<int:angajat_id>/', views.pontaj_lunar_angajat_view, name='pontaj_lunar_angajat'),
    path('status_angajat_update/<int:pk>/', views.StatusAngajatUpdateView.as_view(), name='status_angajat_update'),
    path('absente_angajat/<int:angajat_id>', views.absente_angajat_view, name='absente_angajat'),


    path('create_contract_angajat/<int:employee_id>/', views.create_contract_angajat_view,
         name='create_contract_angajat'),
    path('contracts_all/<int:angajat_id>/', views.contracts_all_view, name='contracts_all'),
    path('contracts_all_employees/', views.contracts_all_employees_view, name='contracts_all_employees'),
    path('update_contract/<int:pk>/', views.ContractUpdateView.as_view(), name='update_contract'),


    path('create_status/', views.create_status_view, name='create_status'),
    path('status_all/', views.status_all_view, name='status_all'),
    path('status_detail/<int:pk>/', views.StatusDetailView.as_view(), name='status_detail'),
    path('update_status/<int:pk>/', views.StatusUpdateView.as_view(), name='update_status'),
    path('delete_status/<int:pk>/', views.StatusDeleteView.as_view(), name='delete_status'),

    path('create_functie/', views.create_functie_view, name='create_functie'),
    path('functie_all/', views.functie_all_view, name='functie_all'),
    path('functie_detail/<int:pk>/', views.FunctieDetailView.as_view(), name='functie_detail'),
    path('update_functie/<int:pk>/', views.FunctieUpdateView.as_view(), name='update_functie'),
    path('delete_functie/<int:pk>/', views.FunctieDeleteView.as_view(), name='delete_functie'),

    path('create_companie/', views.create_companie_view, name='create_companie'),
    path('companie_detail/<int:pk>/', views.CompanieDetailView.as_view(), name='companie_detail'),
    path('update_companie/<int:pk>/', views.CompanieUpdateView.as_view(), name='update_companie'),
    path('delete_companie/<int:pk>/', views.CompanieDeleteView.as_view(), name='delete_companie'),

    path('create_comment/<int:employee_id>/', views.create_comment_view, name='create_comment'),
    path('comments_angajat/<int:angajat_id>/', views.comments_angajat_view, name='comments_angajat'),
    path('comment_update/<int:pk>/', views.CommentUpdateView.as_view(), name='comment_update'),

    path('alte_gestiuni/', views.alte_gestiuni, name='alte_gestiuni'),

    # path('select_status_period/<int:angajat_id>', views.select_status_period_view, name='select_status_period'),

]

# path('creare_angajat/', views.angajat_create_and_register_view, name='create_register'),
# path('create_contract_details/<int:employee_id>/', views.angajat_contract_and_status_create_view, name='create_contract_details'),
# path('update_contract_status/<int:employee_id>/', views.status_and_contract_update_view, name='update_contract_status'),
# path('update_contract/<int:employee_id>/', views.contract_update_view, name='update_contract'),
# path('update_status/<int:status_id>/', views.status_update_view, name='update_status'),
# path('calendar/', views.CalendarView.as_view(), name='calendar'),
# path('pontaj_all/', views.pontaj_all_view, name='pontaj_all'),
# path('pontaj_list/', views.pontaj_list, name='pontaj_list'),
# path('calendar/', views.CalendarView.as_view(), name='calendar'),

# path('staff_view/', views.staff_homepage_view, name='staff_view'),
# path('hello/', views.hello, name='hello'),
# path('home', views.hello, name='home'), probat si asa dar cand dam run si apoi click pe link jos, in run
# apoi pe pagina ce se deschide adaugam in url "/home" - primul parametru din path
# path('', views.HomePageView.as_view(), name='home'),
# path('user_detail/', views.UsersPageView.as_view(), name='detail_user'),
# path('delete_angajat/<int:pk>/', views.EmployeeDeleteView.as_view(), name='delete_angajat'),
# path('employee_details/<int:pk>/', views.EmployeeDetailsView.as_view(), name='details employee'),
# primul parametru din path e ceea ce scriem in link daca vrem sa intram
# pe un anume angajat direct din link(ex:in link avem doar http://127.0.0.1:8001/
# adaugam "/detalii_angajat/<id-ul angajatului dorit>"
# (sau ce ne apare daca dam click pe un angajat)
## al 2-lea parametru este referinta la view-ul dorit, daca e o clasa se trece si ".as_view()",
## daca e functie doar views."nume_functie"
### al 3-lea parametru e NUMELE UNIC pe care trebuie sa il atribuim si care
### apoi il putem folosi in template ca si url, ex:
### href="{%  url 'angajat_detalii' angajat.id %}" - vezi template 'homepage.html'
