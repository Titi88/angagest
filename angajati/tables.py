# https://django-tables2.readthedocs.io/en/latest/pages/custom-data.html
# http://pretagteam.com/question/how-to-export-information-display-in-table-to-excel-django

import django_tables2 as tables
from angajati.models import *

class PontajTable(tables.Table):
    class Meta:
        model = StatusAngajat
        exclude = ("id",)
        # sequence = ('angajat', 'date_time_in', )