import csv
import os
from collections import Counter

import django
import pandas as pandas
from django.db import IntegrityError
from django.urls import reverse_lazy, reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
import datetime
from django.utils import timezone
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin

from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import render, redirect
from django.utils.encoding import force_text
from django.views import generic
from django.views.generic import *
from tablib import Dataset
from angajati import utils
from angajati.forms import *
from angajati.models import *
from angajati.utils import Calendar, generate_month_calendar, ore_lucrate_ok, ore_lucrate_ok_angajat
from angajati.resources import *
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist


class EmployeeMixin:
    model = Employee
    form_class = AngajatForm


class StaffRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_staff

    """
    suprascirem functia test_func(self) din clasa UserPassesTestMixin;
    Deny a request with a permission error if the test_func() method returns
    False.
    """


class StaffStatusDecorator:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        request = args[0]
        if request.user.is_staff:
            result = self.func(*args, **kwargs)
            return result
        else:
            return HttpResponseForbidden()


def home_page_view(request):
    if request.method == 'POST':
        angajat_id = request.POST.get('angajat_id')
        activate = request.POST.get('activate')
        employee = Employee.objects.get(id=angajat_id)

        user = employee.user
        user.is_active = True
        user.save()

    all_employees = Employee.objects.all()
    date_companii = Company.objects.all()

    angajati_cu_status = []
    total_angajati_activi = 0
    for angajat in all_employees:
        last_status = StatusAngajat.objects.filter(angajat=angajat).order_by('id').last()
        if last_status.status.abbreviated_value == 'LCD':
            pass
        else:
            angajati_cu_status.append([angajat, last_status])

        total_angajati_activi = len(angajati_cu_status)

    contract_details = None
    if request.user.is_authenticated:
        angajat = Employee.objects.get(user_id=request.user.id)
        contract_details = ContractDetail.objects.filter(angajat=angajat).first()
    else:
        angajat = None
    return render(request, 'pages/homepage.html',
                  {'total_angajati_activi': total_angajati_activi, 'companii': date_companii, 'angajat': angajat,
                   'contract_details': contract_details})


@StaffStatusDecorator
def create_angajat_view(request):
    if request.method == 'POST':
        angajat_form = AngajatForm(data=request.POST)
        if angajat_form.is_valid():
            angajat: Employee = angajat_form.save()
            angajat.save()

            if '_create' in request.POST:
                return redirect('/')
            elif '_continue' in request.POST:
                return redirect(f"/create_contract_angajat/{angajat.id}/")
            else:
                return redirect(f"/creaza_angajat/")

    else:  # method=get
        # instantiem 2 formulare goale si le trimitem catre template a1b2c3d4.
        angajat_form = AngajatForm()
        user_form = UserCreationForm()

    # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
    return render(request, 'pages/create_angajat.html',
                  {'angajat_form': angajat_form})


# TODO nu merge pentru date fields, de rezolvat ca sa pot importa si data angajare si nastere
def upload_angajati_view(request):
    print(request.GET)
    print(request.POST)
    print(request.FILES)
    if request.method == 'POST':
        try:
            employee_resource = EmployeeResource()
            dataset = Dataset()
            new_employees = request.FILES['myfile']

            imported_data = dataset.load(new_employees.read())
            # result = employee_resource.import_data(dataset, raise_errors=True, dry_run=True)  # Test the data import
            result = employee_resource.import_data(dataset, dry_run=True)  # Test the data import
            if not result.has_errors():
                employee_resource.import_data(dataset, dry_run=False)  # Actually import now
                print(f"a mers import")
                # return redirect('home')

            else:
                response = HttpResponse()
                response.write("<h2>Ceva nu a mers bine!</h2>")
                response.write("<p>Posibile cauze:</p>")
                response.write("<p>1. Id pontaj deja existent la alt angajat, </p>")
                response.write("<p>  sau unul din angajatii din tabel a mai fost introdus o data </p>")
                response.write("<p>2. Revedeti structura coloanelor si denumirea lor in fisierul incarcat</p>")
                response.write("<p>  sau descarcati din pagina anterioara file-ul cu modelul gol de tabel</p>")
                return response
        except MultiValueDictKeyError:
            response = HttpResponse()
            response.write("<h2>Nu ati selectat niciun fisier!</h2>")
            return response
            # return redirect('upload_angajati')

    return render(request, 'pages/upload_angajati.html')


@StaffStatusDecorator
def employee_details_view(request, angajat_id):
    angajat = Employee.objects.get(id=angajat_id)
    statuses = StatusAngajat.objects.filter(angajat_id=angajat_id)

    last_status = statuses.first()
    detalii = ContractDetail.objects.filter(angajat_id=angajat_id).order_by('data_inceperii').reverse()
    last_detail = ContractDetail.objects.filter(angajat_id=angajat_id).last()
    comentarii = Comment.objects.filter(angajat_id=angajat_id).all()
    return render(request, 'pages/employee_detail.html', {'angajat': angajat, 'statuses': statuses, 'detalii': detalii,
                                                          'last_status': last_status, 'comentarii': comentarii,
                                                          'last_detail': last_detail})


@StaffStatusDecorator
@permission_required('angajati.view_employee')
def employee_all_view(request):
    all_employees = Employee.objects.all()
    table_body = Employee.objects.values('nume_angajat', 'prenume_angajat', 'id_pontaj', 'cnp', 'telefon', 'email',
                                         'data_nasterii', 'religie', 'data_angajare', 'data_introducere')

    angajati_cu_status = []
    # total_angajati_activi = 0
    for angajat in all_employees:
        last_status = StatusAngajat.objects.filter(angajat=angajat).order_by('id').last()
        # if last_status.status.abbreviated_value == 'LCD':
        #     pass
        # else:
        angajati_cu_status.append([angajat, last_status])
        # total_angajati_activi = len(angajati_cu_status)

    table_head = [item.capitalize().replace('_', ' ') for item in table_body[0].keys() if item != 'id']

    return render(request, 'pages/employee_all_view.html', {'all_employees': all_employees, 'table_body': table_body,
                                                            'table_head': table_head,
                                                            'angajati_cu_status': angajati_cu_status})


@StaffStatusDecorator
@permission_required('angajati.view_employee')
def employees_actives_view(request):
    all_employees = Employee.objects.all()
    table_body = Employee.objects.values('nume_angajat', 'prenume_angajat', 'id_pontaj', 'cnp', 'telefon', 'email',
                                         'data_nasterii', 'religie', 'data_angajare', 'data_introducere')

    angajati_cu_status = []
    for angajat in all_employees:
        last_status = StatusAngajat.objects.filter(angajat=angajat).order_by('id').last()
        if last_status is None:
            status_set = Status.objects.get(abbreviated_value='ANG')
            new_status = StatusAngajat.objects.create(angajat=angajat, status=status_set)
            angajati_cu_status.append([angajat, new_status])
        elif last_status.status.abbreviated_value == 'LCD':
            pass
        else:
            angajati_cu_status.append([angajat, last_status])
        total_angajati_activi = len(angajati_cu_status)

    table_head = [item.capitalize().replace('_', ' ') for item in table_body[0].keys() if
                  item != 'id']

    return render(request, 'pages/employees_actives.html', {'all_employees': all_employees, 'table_body': table_body,
                                                            'table_head': table_head,
                                                            'angajati_cu_status': angajati_cu_status,
                                                            'total_angajati_activi': total_angajati_activi})


class EmployeeUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_employee'
    model = Employee
    form_class = AngajatForm
    template_name = 'pages/update_employee.html'
    success_url = '/'


@StaffStatusDecorator
@permission_required('angajati.change_user')
def disable_employee_view(request, pk):
    employee = Employee.objects.get(id=pk)
    status_employee = StatusAngajat.objects.filter(angajat=employee).last()
    toti = Employee.objects.all()
    contract_angajat = ContractDetail.objects.filter(angajat_id=pk).last()
    try:
        status_set = Status.objects.get(abbreviated_value='LCD')
    except ObjectDoesNotExist:
        Status.objects.create(view_value="LICHIDAT", abbreviated_value="LCD")
        status_set = Status.objects.get(abbreviated_value='LCD')

    if request.method == 'POST':
        # if status_employee is None or status_employee.status.abbreviated_value != 'LCD':
        if status_employee.status.abbreviated_value != 'LCD':
            status_nou = StatusAngajat.objects.create(angajat=employee, status=status_set)
            comentariu_nou = Comment.objects.create(angajat=employee,
                                                    continut=f"angajat lichidat/ dezactivat in {datetime.now()}")
            nume_nou = f"(Lichidat) {employee.nume_angajat}"
            Employee.objects.filter(id=employee.id).update(nume_angajat=nume_nou)
            try:
                contract_angajat.data_incetarii = datetime.now()
            except AttributeError:
                pass

        # else:
        #     comentariu_nou = Comment.objects.create(angajat=employee,
        #                                             continut=f"dezactivat in {datetime.now()}, angajatul era deja Lichidat")
        try:
            user = employee.user
            user.is_active = False
            user.save()
        except AttributeError:
            pass
        return redirect('/')
    return render(request, 'pages/delete_employee.html', {'employee': employee, 'toti': toti})


def export_csv_angajati(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Lista angajati_{}.csv"'.format(date.today())
    table_body = Employee.objects.values('nume_angajat', 'prenume_angajat', 'id_pontaj', 'telefon', 'email',
                                         'data_nasterii',
                                         'religie', 'data_angajare', 'data_introducere', 'id')
    writer = csv.DictWriter(response, fieldnames=['nume_angajat', 'prenume_angajat', 'id_pontaj', 'telefon', 'email',
                                                  'data_nasterii',
                                                  'religie', 'data_angajare', 'data_introducere', 'id'])

    writer.writeheader()
    writer.writerows(table_body)

    return response


# TODO fisierul descarcat nu merge mai apoi sa ii fac upload!!!
def download_file_for_upload_angajati(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="lista_angajati_pentru_import.csv"'
    table_body = Employee.objects.values('nume_angajat', 'prenume_angajat', 'id_pontaj', 'cnp', 'telefon', 'email',
                                         'religie')
    writer = csv.DictWriter(response,
                            fieldnames=['nume_angajat', 'prenume_angajat', 'id_pontaj', 'cnp', 'telefon', 'email',
                                        'religie'])
    writer.writeheader()

    return response


# TODO pt export view - csv writer - configurabil pe angajat, interval
# INFO: logica upload_pontaj - incarca fisierul din programul de pontaj si creeaza status_angajat in baza id_pontaj
# pt fiecare angajat din baza de date la care se regaseste id_pontaj din fisierul de upload/ de pontaj
@StaffStatusDecorator
def upload_pontaj_csv_view(request):
    print(request.GET)
    print(request.POST)
    print(request.FILES)
    try:
        if request.method == 'POST' and request.FILES['csv_file']:
            csv_file = request.FILES['csv_file']  # l-am citit din dictionar
            fs = FileSystemStorage(location='pontaje_salvate')
            # filename = fs.save(csv_file.name, csv_file)
            if fs.exists(csv_file.name):
                print(f"existing filename: {csv_file.name}")
                fs.delete(csv_file.name)
                print(f"deleted {csv_file.name}")
                new_filename = fs.save(csv_file.name, csv_file)
                print(f"new filename: {new_filename}")
            else:
                new_filename = fs.save(csv_file.name, csv_file)
                print(f"else new filename: {new_filename}")

            # Look to the path of your current working directory
            working_directory = os.getcwd()
            print(f"working dir {working_directory}")
            file_path = working_directory + f"/pontaje_salvate/{new_filename}"
            print(f"filepath: {file_path}")

            # with open(f"../pontaje_salvate/{new_filename}") as csv_file:
            with open(file_path) as csv_file:
                csv_reader = csv.reader(csv_file)
                line_count = 0
                for row in csv_reader:
                    line_count += 1
                    if line_count < 17:
                        continue

                    row_type = row[0]
                    if row_type == "0":
                        employee_details = row[1]
                        id_start_index = employee_details.find('ID: ') + len("ID: ")
                        id_stop_index = employee_details.find(')', id_start_index)
                        id_pontaj_angajat = employee_details[id_start_index:id_stop_index]
                        angajat = Employee.objects.filter(id_pontaj=id_pontaj_angajat).first()

                    elif row_type == "1":
                        _, date, hours_worked, first_in, last_out = row

                        if " - " in date:  # cazul in care avem 2 date, intrat azi, iesit maine:
                            dates = date.split(" - ")
                            date1 = dates[0]
                            date_time_in = datetime.strptime(f"{dates[0]} {first_in}", '%Y/%m/%d %H:%M:%S')
                            date_time_out = datetime.strptime(f"{dates[1]} {last_out}", '%Y/%m/%d %H:%M:%S')
                        else:  # avem 1 data
                            try:
                                date1 = date
                                # date_time_in = datetime.strptime(f"{date} {first_in}", '%d/%m/%Y %H:%M:%S')
                                date_time_in = datetime.strptime(f"{date} {first_in}", '%Y/%m/%d %H:%M:%S')
                                # date_time_out = datetime.strptime(f"{date} {last_out}", '%d/%m/%Y %H:%M:%S')
                                date_time_out = datetime.strptime(f"{date} {last_out}", '%Y/%m/%d %H:%M:%S')
                            except ValueError:
                                date_time_in = datetime.strptime(f"{date} {first_in}", '%d/%m/%Y %H:%M:%S')
                                date_time_out = datetime.strptime(f"{date} {last_out}", '%d/%m/%Y %H:%M:%S')

                        angajat_status = Status.objects.filter(abbreviated_value='ANG').first()
                        if angajat is None:  # daca nu gaseste angajatul dupa id-ul din tabelul incarcat, trece peste
                            # acel angajat in citirea tabelului
                            continue

                        existing_date_status = StatusAngajat.objects.filter(angajat=angajat,
                                                                            date_time_in__year=date_time_in.year,
                                                                            date_time_in__month=date_time_in.month,
                                                                            date_time_in__day=date_time_in.day)
                        if existing_date_status.count() > 0:  # daca deja exista o intrare pentru angajatul respectiv
                            # merge mai departe, NU O SUPRASCRIE
                            continue
                        # mai jos se creeaza intrarea in baza de date
                        status_angajat = StatusAngajat.objects.create(status=angajat_status, angajat=angajat,
                                                                      date_time_in=date_time_in,
                                                                      date_time_out=date_time_out)
                    # print(f"row{row}")
                # print(f'Processed {line_count} lines.')
            # print(f"deleting file: {filename}")
            # filename = fs.delete(csv_file.name)
            # print(f"deleted")

    except MultiValueDictKeyError:
        # print("MultiValueDictKeyError: nu s-a selectat niciun fisier pentru incarcare")
        response = HttpResponse()
        response.write("<p><br/>Nu s-a selectat niciun fisier pentru incarcare!</p>")
        response.write("<p><br/>Reveniti la meniul anterior si alegeti prima data un fisier, apoi apasati "
                       "butonul 'Upload csv for processing'!</p>")
        return response


    return render(request, 'pages/upload_pontaj_csv.html', {})


@StaffStatusDecorator
@permission_required('angajati.view_statusangajat')
def pontaj_lunar_view(request):
    # month_ = int(request.GET.get('month_name', timezone.now().month))
    month_ = int(request.GET.get('month_name', datetime.now().month))
    # year_ = int(request.GET.get('year_value', timezone.now().year))
    year_ = int(request.GET.get('year_value', datetime.now().year))

    # django.utils.timezone.now() de incerca sa il folosesc mai sus!!!

    list_month, month_list, month_name, week_days = generate_month_calendar(year_, month_)
    statusuri_angajat = StatusAngajat.objects.filter(date_time_in__month=month_, date_time_in__year=year_).order_by('date_time_in').all()

    dict_pontaj = {}
    for status_angajat in statusuri_angajat:
        if status_angajat.angajat in dict_pontaj:
            dict_pontaj[status_angajat.angajat].append(status_angajat)
        elif status_angajat.angajat is None:
            pass
        else:
            dict_pontaj[status_angajat.angajat] = []
            dict_pontaj[status_angajat.angajat].append(status_angajat)
    print(f"dict pontaj: {dict_pontaj}")

    lista_pontaj = []
    for angajat, status_list in dict_pontaj.items():
        print(f"dict items: {dict_pontaj.items()}")
        print(f"status list: {status_list}")

        lista_pontaj.append([angajat, status_list])

    print(f"lista pontaj: {lista_pontaj}")

    lista_angajati_cu_ore = ore_lucrate_ok(lista_pontaj)

    table_head = ["ID Pontaj", "Nume", "Prenume"]

    return render(request, 'pages/pontaj_lunar.html',
                  {'year_': year_, 'month_name': month_name, 'month_nr': month_, 'table_head': table_head,
                   'month_list': month_list, 'date_angajat': statusuri_angajat, 'lista_pontaj': lista_pontaj,
                   'week_days': week_days, 'list_month': list_month, 'dict_pontaj': dict_pontaj,
                   'lista_angajati_cu_ore': lista_angajati_cu_ore})


def pontaj_lunar_angajat_view(request, angajat_id):
    angajat = Employee.objects.get(id=angajat_id)
    month_ = int(request.GET.get('month_name', datetime.now().month))
    year_ = int(request.GET.get('year_value', datetime.now().year))
    all_statusuri_angajat = StatusAngajat.objects.all().filter(angajat=angajat, date_time_in__year=year_)
    statusuri_luna_angajat = StatusAngajat.objects.all().filter(angajat=angajat, date_time_in__month=month_,
                                                                date_time_in__year=year_)

    list_month, month_list, month_name, week_days = generate_month_calendar(year_, month_)

    lista_pontaj_lunar = [angajat, []]
    for status_angajat in statusuri_luna_angajat:
        lista_pontaj_lunar[1].append(status_angajat)
    lista_angajat_cu_ore_lunar = ore_lucrate_ok_angajat(lista_pontaj_lunar)
    lista_lunar_zip = zip(lista_pontaj_lunar[1], lista_angajat_cu_ore_lunar[1])

    lista_pontaj_total = [angajat, []]
    for status_angajat in all_statusuri_angajat:
        lista_pontaj_total[1].append(status_angajat)
    lista_angajat_cu_ore_total = ore_lucrate_ok_angajat(lista_pontaj_total)
    lista_total_zip = zip(lista_pontaj_total[1], lista_angajat_cu_ore_total[1])

    return render(request, 'pages/pontaj_lunar_angajat.html',
                  {'year_': year_, 'month_name': month_name, 'month_nr': month_,
                   'month_list': month_list, 'statusuri_luna_angajat': statusuri_luna_angajat,
                   'lista_pontaj_lunar': lista_pontaj_lunar, 'lista_pontaj_total': lista_pontaj_total,
                   'lista_angajat_cu_ore_lunar': lista_angajat_cu_ore_lunar,
                   'lista_angajat_cu_ore_total': lista_angajat_cu_ore_total,
                   'lista_total_zip': lista_total_zip, 'lista_lunar_zip': lista_lunar_zip,
                   'angajat': angajat, 'week_days': week_days, 'list_month': list_month})


@StaffStatusDecorator
@permission_required('angajati.view_statusangajat')
def pontaj_simplificat_view(request):
    month_ = int(request.GET.get('month_name', datetime.now().month))
    year_ = int(request.GET.get('year_value', datetime.now().year))

    list_month, month_list, month_name, week_days = generate_month_calendar(year_, month_)
    statusuri_angajat = StatusAngajat.objects.filter(date_time_in__month=month_, date_time_in__year=year_).all()

    dict_pontaj = {}
    for status_angajat in statusuri_angajat:
        if status_angajat.angajat in dict_pontaj:
            dict_pontaj[status_angajat.angajat].append(status_angajat)
        elif status_angajat.angajat is None:
            pass
        else:
            dict_pontaj[status_angajat.angajat] = []

            dict_pontaj[status_angajat.angajat].append(status_angajat)

    lista_pontaj = []
    for angajat, status_list in dict_pontaj.items():
        lista_pontaj.append([angajat, status_list])

    lista_angajati_cu_ore = ore_lucrate_ok(lista_pontaj)

    table_head = ["ID Pontaj", "Nume", "Prenume"]

    return render(request, 'pages/pontaj_simplificat.html',
                  {'year_': year_, 'month_name': month_name, 'month_nr': month_, 'table_head': table_head,
                   'month_list': month_list, 'date_angajat': statusuri_angajat, 'lista_pontaj': lista_pontaj,
                   'week_days': week_days, 'list_month': list_month, 'dict_pontaj': dict_pontaj,
                   'lista_angajati_cu_ore': lista_angajati_cu_ore})


def absente_angajat_view(request, angajat_id):
    angajat = Employee.objects.get(id=angajat_id)
    month_ = int(request.GET.get('month_name', datetime.now().month))
    year_ = int(request.GET.get('year_value', datetime.now().year))
    statusuri_luna_angajat = StatusAngajat.objects.all().filter(angajat=angajat, date_time_in__month=month_,
                                                                date_time_in__year=year_)
    list_month, month_list, month_name, week_days = generate_month_calendar(year_, month_)

    lista_absente_lunar = [month_name, []]
    for status_angajat in statusuri_luna_angajat:
        if status_angajat.status.abbreviated_value != 'ANG':
            lista_absente_lunar[1].append(status_angajat)

    all_statusuri_angajat = StatusAngajat.objects.all().filter(angajat=angajat, date_time_in__year=year_)

    month_idx = 1
    lista_absente_total = []

    # INFO: logica lista_lunara:[index luna, [lista statusuri diferite de ANG],
    # {dictionar cu count pe fiecare tip de status}, totalul elementelor din lista statusuri diferite de ANG]
    lista_an_absente = []
    while month_idx < 13:
        lista_lunara = [0, [], {}, 0]
        for angajat in all_statusuri_angajat:
            if angajat.date_time_in.astimezone().month == month_idx:
                if angajat.status.abbreviated_value != 'ANG':
                    lista_lunara[0] = month_idx
                    lista_lunara[1].append(angajat.status.view_value)
                    lista_an_absente.append(angajat.status.view_value)

            else:
                lista_lunara[0] = month_idx
        dict_counter_status = dict(Counter(lista_lunara[1]))
        lista_lunara[2] = dict_counter_status
        lista_lunara[3] = len(lista_lunara[1])
        lista_absente_total.append(lista_lunara)
        month_idx += 1

    dict_anual_absente = dict(Counter(lista_an_absente))

    total_zile_absente_an = 0
    for total_lunar in lista_absente_total:
        total_zile_absente_an += total_lunar[3]

    print(f"tot: {total_zile_absente_an}")

    return render(request, 'pages/absente_angajat.html',
                  {'year_': year_, 'month_name': month_name, 'month_nr': month_,
                   'month_list': month_list, 'angajat': angajat, 'week_days': week_days,
                   'list_month': list_month, 'lista_absente_lunar': lista_absente_lunar,
                   'lista_absente_total': lista_absente_total, 'total_zile_absente_an': total_zile_absente_an,
                   'all_statusuri_angajat': all_statusuri_angajat, 'dict_anual_absente': dict_anual_absente})


# TODO create_status_angajat_view care sa imi poate face de ex
#  concediu pe mai multe zile si sa imi salveze pe zilele respective acel status de concediu
class StatusAngajatUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_statusangajat'
    model = StatusAngajat
    form_class = StatusAngajatUpdateForm
    template_name = 'pages/status_angajat_update.html'

    # success_url = '/'

    def get_success_url(self):
        return reverse("pontaj_lunar")


# INFO: logica create_contract: fiecare contract nou inchide ultimul contract, cu end date automat, fara sa il stearga
@StaffStatusDecorator
@permission_required('angajati.add_contractdetail')
def create_contract_angajat_view(request, employee_id):
    employee = Employee.objects.get(id=employee_id)

    if request.method == 'POST':
        contract_form = ContractDetailCreateForm(data=request.POST)
        if contract_form.is_valid():
            last_contract = ContractDetail.objects.filter(angajat=employee).last()
            if last_contract:
                last_contract.data_incetarii = datetime.now()
                last_contract.save()
            contract: ContractDetail = contract_form.save()
            contract.angajat_id = employee_id
            contract.save()
            return redirect(f"/employee_details/{employee_id}")
    else:
        contract_form = ContractDetailCreateForm()

    return render(request, 'pages/create_contract_angajat.html', {'employee': employee, 'contract_form': contract_form})


def contracts_all_employees_view(request):
    toti_angajatii = Employee.objects.all()
    last_contract_details = []
    toate_functiile = []
    for angajat in toti_angajatii:
        actual_contract_details = ContractDetail.objects.filter(angajat=angajat).last()
        last_contract_details.append([angajat, actual_contract_details])
        if actual_contract_details is None:
            toate_functiile.append("[LIPSA CONTRACT]")
        else:
            toate_functiile.append(actual_contract_details.functie)

    dict_totaluri_functii = dict(Counter(toate_functiile))

    return render(request, 'pages/contracts_all_employees.html',
                  {'last_contract_details': last_contract_details, 'dict_totaluri_functii': dict_totaluri_functii})


def contracts_all_view(request, angajat_id):
    all_contracts_angajat = ContractDetail.objects.all().filter(angajat_id=angajat_id)
    angajat = Employee.objects.get(id=angajat_id)
    return render(request, 'pages/contracts_all.html',
                  {'contracts': all_contracts_angajat, 'angajat': angajat})


class ContractUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_contractdetail'
    model = ContractDetail
    form_class = ContractDetailUpdateForm
    template_name = 'pages/update_contract.html'
    success_url = '/'

    # def get_success_url(self, **kwargs):
    #     return reverse("details_employee", kwargs={'pk': self.object.pk})


@StaffStatusDecorator
def create_comment_view(request, employee_id):
    employee = Employee.objects.get(id=employee_id)
    if request.method == 'POST':
        comment_form = CommentFormAngajat(data=request.POST)
        if comment_form.is_valid():
            comment: Comment = comment_form.save()
            # comment.angajat = employee
            comment.angajat_id = employee_id
            comment.nume_autor = request.user
            comment.save()
            return redirect(f"/employee_details/{employee_id}")
    else:
        try:
            comment_form = CommentFormAngajat()
        except ObjectDoesNotExist:
            response = HttpResponse()
            response.write("<h2>Nu exista angajat cu acest id</h2>")
            return response

    return render(request, 'pages/create_comment.html', {'employee': employee, 'comment_form': comment_form})


def comments_angajat_view(request, angajat_id):
    all_comments_angajat = Comment.objects.all().filter(angajat_id=angajat_id)
    angajat = Employee.objects.get(id=angajat_id)
    return render(request, 'pages/comments_angajat.html',
                  {'all_comments_angajat': all_comments_angajat, 'angajat': angajat})


class CommentUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_comment'
    model = Comment
    form_class = CommentFormAngajat
    template_name = 'pages/comment_update.html'
    success_url = '/employees_actives/'


@StaffStatusDecorator
def create_status_view(request):
    if request.method == 'POST':
        status_form = StatusForm(data=request.POST)
        if status_form.is_valid():
            status_form.save()
            if '_create' in request.POST:
                return redirect('/')
            elif '_continue' in request.POST:
                return redirect(f"/create_status/")

    else:
        status_form = StatusForm()

    return render(request, 'pages/create_status.html', {'status_form': status_form})


@StaffStatusDecorator
def status_all_view(request):
    all_status = Status.objects.all()
    return render(request, 'pages/status_all_view.html', {'statuses': all_status})


class StatusDetailView(PermissionRequiredMixin, StaffRequiredMixin, DetailView):
    permission_required = 'angajati.view_status'
    model = Status
    template_name = 'pages/status_detail.html'
    context_object_name = 'status'


class StatusUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_status'
    model = Status
    form_class = StatusForm
    template_name = 'pages/status_update.html'
    success_url = '/status_all/'


class StatusDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'angajati.delete_status'
    model = Status
    template_name = 'pages/status_delete.html'
    success_url = '/status_all/'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        status = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['status'] = status  # pasam status-ul pe cheia status
        return context


@StaffStatusDecorator
def create_functie_view(request):
    if request.method == 'POST':
        functie_form = FunctieForm(data=request.POST)
        if functie_form.is_valid():
            functie_form.save()
            if '_create' in request.POST:
                return redirect('/')
            elif '_continue' in request.POST:
                return redirect(f"/create_functie/")

    else:
        functie_form = FunctieForm()

    return render(request, 'pages/create_functie.html', {'functie_form': functie_form})


@StaffStatusDecorator
def functie_all_view(request):
    all_functii = Functie.objects.all()
    return render(request, 'pages/functie_all_view.html', {'functii': all_functii})


class FunctieDetailView(PermissionRequiredMixin, StaffRequiredMixin, DetailView):
    permission_required = 'angajati.view_functie'
    model = Functie
    template_name = 'pages/functie_detail.html'
    context_object_name = 'functie'


class FunctieUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_functie'
    model = Functie
    form_class = FunctieForm
    template_name = 'pages/functie_update.html'
    success_url = '/functie_all/'


class FunctieDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'angajati.delete_functie'
    model = Functie
    template_name = 'pages/functie_delete.html'
    success_url = '/functie_all/'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        functie = self.object
        context['functie'] = functie
        return context


# logica create_companie: doar 1 singura companie poate exista
@StaffStatusDecorator
def create_companie_view(request):
    companii = list(Company.objects.all())
    if request.method == 'POST' and len(companii) == 0:
        companie_form = CompanyForm(data=request.POST)
        if companie_form.is_valid():
            companie_form.save()
            return redirect('/')
    elif request.method == 'POST' and len(companii) >= 1:
        # https://docs.djangoproject.com/en/3.2/ref/request-response/
        response = HttpResponse()
        response.write("<p>Puteti adauga o singura companie!</p>")
        response.write("<p>Daca doriti, puteti sa stergeti compania actuala</p>")
        response.write("<p>si sa creati una noua!</p>")
        response.write("<p>Apasati pe link-ul de mai jos pentru a reveni la meniul principal</p>")
        response.write(
            "<a class='btn btn-success mt-1' style='color: black; background-color: #97d2f3'href='/' role='button'>Inapoi la ecranul principal</a>")
        return response
    else:
        companie_form = CompanyForm()

    return render(request, 'pages/create_companie.html', {'companie_form': companie_form})


class CompanieDetailView(PermissionRequiredMixin, StaffRequiredMixin, DetailView):
    permission_required = 'angajati.view_company'
    model = Company
    template_name = 'pages/companie_detail.html'
    context_object_name = 'companie'


class CompanieUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'angajati.change_company'
    model = Company
    form_class = CompanyForm
    template_name = 'pages/companie_update.html'
    success_url = '/'


class CompanieDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'angajati.delete_company'
    model = Company
    template_name = 'pages/companie_delete.html'
    success_url = '/create_companie/'

    def get_context_data(self,
                         **kwargs):
        context: dict = super().get_context_data(
            **kwargs)
        companie = self.object
        context['companie'] = companie
        return context


@StaffStatusDecorator
def alte_gestiuni(request):
    date_companii = Company.objects.all()

    return render(request, 'pages/alte_gestiuni.html', {'companii': date_companii})

# def ore_lucrate(lista_primita):
#     # for angajat_statuses in lista_primita:
#     #     for status in angajat_statuses:
#         for status in lista_primita:
#             # print(status.date)
#             # print(status.last_out)
#             print(f"out functie:{status.date}:{status.last_out}")
#             print(f"{status.date}:{status.first_in}")
#             print(status.status)
#             # _in = status.first_in
#             # print(f"in: {_in}")
#             # print(f"type: {type(_in)}")
#             # _out = datetime.time(status.last_out.hour)
#             # print(f"out: {_out}")
#             # t = _in-_out
#             # print(f"t: {t}")
#             ore_lucrate = datetime.combine(date.min, status.last_out) - datetime.combine(date.min, status.first_in)
#             print(f"ore lucrate: {ore_lucrate}, {status.angajat}")
#             return ore_lucrate


# def pontaj_all_view(request):
#     toti_angajatii = Employee.objects.all()
#     all_statusuri = list(StatusAngajat.objects.all().filter(angajat=angajat) for angajat in toti_angajatii)
#     print(toti_angajatii)
#
#     table_head = ['NUME', 'PRENUME', 'INTRAT', 'IESIT', 'ORE LUCRATE']
#
#     return render(request, 'pages/pontaj_all.html',
#                   {'toti_angajatii': toti_angajatii, 'table_head': table_head,
#                    'all_statusuri': all_statusuri})
# # ### TODO
#
# def save_and_export_ore_lucrate_view(request):
#     # zile/ ore lucratoare per luna
#     pass

# ### TODO
# dupa save and export, facem o functie care sa stearga baza de date la o anumit perioada

# TODO de citit
## http://pretagteam.com/question/how-to-export-information-display-in-table-to-excel-django
## https://www.youtube.com/watch?v=ai9J71DlLhk
## https://stackoverflow.com/questions/58010268/how-to-export-information-display-in-table-to-excel-django


# #
# def get_date(req_day):
#     if req_day:
#         year, month = (int(x) for x in req_day.split('-'))
#         return date(year, month, day=1)
#     return datetime.today()
# #
# class CalendarView(generic.ListView):
#     model = StatusAngajat
#     template_name = 'pages/calendar.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#
#         # use today's date for the calendar
#         d = get_date(self.request.GET.get('day', None))
#
#         # Instantiate our calendar class with today's year and date
#         cal = Calendar(d.year, d.month)
#
#         # Call the formatmonth method, which returns our calendar as a table
#         html_cal = cal.formatmonth(d.year, d.month, withyear=True)
#         context['calendar'] = mark_safe(html_cal)
#         return context


# class Echo:
#     """An object that implements just the write method of the file-like
#     interface.
#     """
#     def write(self, value):
#         """Write the value by returning it, instead of storing in a buffer."""
#         return value
#
# def some_streaming_csv_view(request):
#     """A view that streams a large CSV file."""
#     # Generate a sequence of rows. The range is based on the maximum number of
#     # rows that can be handled by a single sheet in most spreadsheet
#     # applications.
#
#     rows = (["Row {}".format(idx), str(idx)] for idx in range(65536))
#     pseudo_buffer = Echo()
#     writer = csv.writer(pseudo_buffer)
#     return StreamingHttpResponse(
#         (writer.writerow(row) for row in rows),
#         content_type="text/csv",
#         headers={'Content-Disposition': 'attachment; filename="somefilename.csv"'},
#     )


#
# @StaffStatusDecorator
# def staff_homepage_view(request):
#     if request.method == "POST":
#         my_dict = dict(request.POST)
#         checks = my_dict.get('checks', [])
#         for check in checks:
#             Employee.objects.get(id=check).delete()
#
#     table_body = Employee.objects.values('id', 'nume_angajat', 'prenume_angajat', 'statusangajat__status__view_value',
#                                          'contractdetail__functie__view_value', 'contractdetail__salar_brut',
#                                          'contractdetail__naveta',
#                                          'comment__continut', 'user__username')
#
#     # table_head = [item.capitalize().replace('_', ' ') for item in table_body[0].keys() if
#     #                           item != 'id']
#     table_head = ['NUME', 'PRENUME', 'STARE', 'FUNCTIE', 'SALAR BRUT', 'NAVETA', 'COMENTARII', 'USER']
#
#     url_create = 'creare_angajat/'
#     url_update = 'employee_update/'
#     url_delete = 'delete_angajat/'
#
#     return render(request, 'pages/staff_homepage_view.html',
#                   {'table_body': table_body, 'table_head': table_head, 'url_create': url_create,
#                    'url_update': url_update,
#                    'url_delete': url_delete})

#     toti_angajatii = Employee.objects.all()
#
#     view_data = Employee.objects.values('user__username','nume_angajat','prenume_angajat',
#                                         'statusangajat__status__view_value', 'contractdetail__functie__view_value',
#                                         'contractdetail__salar_brut', 'contractdetail__naveta','comment__continut')
#     table_head = ['USER', 'NUME', 'PRENUME', 'STARE', 'FUNCTIE', 'SALAR BRUT', 'NAVETA', 'COMENTARII']
#     last_status = StatusAngajat.objects.all().order_by('status_id')[:2]
#     contract_details = ContractDetail.objects.all()
#
#
#     return render(request, 'partials/staff_homepage_view.html', {'angajati': toti_angajatii, 'statuses': last_status,
#                                                                  'contract_detail':contract_details,
#                                                                  'head_table': table_head})
#


# @StaffStatusDecorator
# @permission_required('angajati.change_statusangajat')
# def status_update_view(request, status_id): # status_pk in clasa
#     status = StatusAngajat.objects.get(id=status_id)
#     status_form = StatusAngajatUpdateForm(instance=status)
#     print(f"status: {status}")
#
#     if request.method == 'POST':
#         status_form = StatusAngajatUpdateForm(request.POST)
#         if status_form.is_valid():
#
#             status = status_form.save()
#             status.save()
#         return redirect(request.META.get('HTTP_REFERER'))
#
#     return render(request, 'pages/update_status.html', {'status': status,'status_form': status_form})


# TODO cauta cum sa completezi fielduri in formular inainte sa il validezi
# @StaffStatusDecorator
# def angajat_contract_and_status_create_view(request, employee_id):
#     if request.method == 'POST':
#         contract_form = ContractDetailUpdateForm(data=request.POST)
#         status_form = StatusAngajatUpdateForm(data=request.POST)
#         if contract_form.is_valid() and status_form.is_valid():
#             contract: ContractDetail = contract_form.save()
#             status: StatusAngajat = status_form.save()
#             contract.angajat_id = employee_id
#             status.angajat_id = employee_id
#             contract.save()
#             status.save()
#             return redirect('/')
#     else:
#         contract_form = ContractDetailUpdateForm()
#         status_form = StatusAngajatUpdateForm()

# return render(request, 'pages/create_contract_and_status_angajat.html',
#               {'status_form': status_form, 'contract_form': contract_form})

# def rectivate_user(request):
#     # am facut formularul direct in templates, in hompage, cu 2 input type hidden si buton de Reactiveaza user
#     pass


# class MultiRedirectMixin(object):
#     success_urls = {}
#
#     def form_valid(self, form):
#         """ Form is valid: Pick the url and redirect.
#         """
#
#         for name in self.success_urls:
#             if name in form.data:
#                 self.success_url = self.success_urls[name]
#                 break
#
#         return HttpResponseRedirect(self.get_success_url())
#
#     def get_success_url(self):
#         """
#         Returns the supplied success URL.
#         """
#         if self.success_url:
#             # Forcing possible reverse_lazy evaluation
#             url = force_text(self.success_url)
#         else:
#             raise ImproperlyConfigured(
#                 _("No URL to redirect to. Provide a success_url."))
#         return url

# TODO de creat o functie ca sa pot baga si al doilea success url cu salveaza si continua sa adaugi altul
# class StatusCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
#     permission_required = 'angajati.add_status'
#     model = Status
#     form_class = StatusForm
#     template_name = 'pages/create_status.html'
#     success_url = '/'

# success_urls = {"create": reverse_lazy('_create'), "continue": reverse_lazy('_continue')}

# # TODO AngajatCreateView - facut mai jos impreuna cu register
# class AngajatCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
#     model = Employee
#     template_name = 'pages/angjat_create.html'
#     form_class = AngajatForm

# def angajat_create_and_register_view(request):
#     if request.method == 'POST':
#         angajat_form = AngajatForm(data=request.POST)
#         user_form = UserCreationForm(data=request.POST)
#         if angajat_form.is_valid() and user_form.is_valid():
#             angajat = angajat_form.save()
#             user = user_form.save()
#             # salvam profilul de angajat si facem un update la coloana user si legam de user-ul creat mai sus
#
#             angajat.user = user
#             angajat.save()
#             return redirect('/')
#     else:  # method=get
#         # instantiem 2 formulare goale si le trimitem catre template
#         angajat_form = AngajatForm()
#         user_form = UserCreationForm()
#
#     # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
#     return render(request, 'pages/create_and_register_angajat.html',
#                   {'angajat_form': angajat_form, 'user_form': user_form})


# class EmployeeDetailsView(DetailView):
#     template_name = 'pages/employee_detail.html'
#     model = Employee
#     context_object_name = 'details_employee'
#
#     def get_context_data(self, **kwargs):
#         # apelam get_context_data al lui DetailView; obtinem dictionarul {"detalii_angajat":...}
#         context = super().get_context_data(**kwargs)
#         detaliu = context[self.context_object_name]  # scoatem din dictionarul de mai sus valoarea
#         # context['comentarii'] = Comment.objects.all() - angajat = col
#         context['comentarii'] = Comment.objects.filter(angajat=detaliu)  # adaugam in dictionarul de context comentariile
#
#         return context

# class UsersPageView(ListView):
#     # template_name = "user_detail_view.html"
#     template_name = "pages/user_detail_view.html"
#     model = User
#     context_object_name = "user_detail"

# clasa de mai jos inlocuita cu def_disable_view
# class EmployeeDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
#     permission_required = 'angajati.delete_employee'
#     model = Employee
#     template_name = 'pages/delete_employee.html'
#     success_url = '/'
#
#     def get_context_data(self,
#                          **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
#         context: dict = super().get_context_data(**kwargs)
#         employee = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
#         context['employee'] = employee  # pasam employee pe cheia employee
#         return context


# @StaffStatusDecorator
# @permission_required('angajati.change_statusangajat')
# def select_status_period_view(request, angajat_id):
#     angajat = Employee.objects.get(id=angajat_id)
#     print("before")
#
#     all_statuses_angajat = StatusAngajat.objects.filter(angajat_id=angajat_id)
#     start_date = all_statuses_angajat.first().date_time_in.date()
#     print(f"start date type: {type(start_date)}")
#     end_date = all_statuses_angajat[4].date_time_in.date()
#     date_range = pandas.date_range(start_date, end_date-timedelta(days=0), freq='d')
#     if request.method == 'POST':
#         for date_ in date_range:
#             status_set = Status.objects.get(abbreviated_value='IP')
#             # print(f"date_.to_pydatetime(): {type(date_.to_pydatetime())}")
#             print(f"date.date: {type(date_.date())}")
#             StatusAngajat.objects.update_or_create(angajat=angajat, status=status_set, date_time_in=date_.date(), date_time_out=date_.date())
#             # !!! A MODIFICAT TOATE STATUSURILE DE LA TOTI ANGAJATII, PARTEA DE MAI JOS
#             # StatusAngajat.objects.update(angajat=angajat, status=status_set, date_time_in=date_.date(), date_time_out=date_.date())
#             print(f"date_ type: {type(date_)}")
#             print(f"date_ type2: {type(date_.date())}")
#         print(f"date range: {date_range}")
#
#
#
#
#     return render(request, 'pages/select_status_period.html', {'angajat':angajat, 'all_statuses_angajat': all_statuses_angajat,
#                                                                'start_date': start_date, 'end_date': end_date})

# angajat_status = StatusAngajat.objects.get(angajat_id=angajat_id)
# date_inceput = []
# date_sfarsit = []
# start_data = ""
# stop_data = ""
# # for dates in
# if request.method == 'POST':
#     status_form = StatusAngajatUpdateForm(data=request.POST)
#     if status_form.is_valid():
#         for status in angajat_status:
#             date_inceput.append(status.date_time_in)
#             date_sfarsit.append(status.date_time_out)
#         if start_data in date_inceput and stop_data in date_sfarsit:
#             for data_noua_in in date_inceput:
#                 data_noua_in = status_form.date_time_in
#                 status_form.save()
#             for data_noua_out in date_sfarsit:
#                 data_noua_out = status_form.date_time_out
#                 status_form.save()
#     return redirect('home')
#
# else:
#     angajat_status = StatusAngajat.objects.filter(status_id=status_id)
#
#     status_form = StatusAngajatUpdateForm()
#
#
#     start_data = "2021-09-01"
#     stop_data = ""
#
# return render(request, 'pages/select_status_period.html', {'status_form': status_form})
#               # {'start_data':start_data,'stop_data': stop_data, 'angajat_status': angajat_status})


#
# @StaffStatusDecorator
# def contract_update_view(request, employee_id):
#     employee = Employee.objects.get(id=employee_id)
#     contract = ContractDetail.objects.filter(angajat=employee).last()
#
#     contract_form = ContractDetailUpdateForm(instance=contract)
#
#     if request.method == 'POST':
#         contract_form = ContractDetailUpdateForm(request.POST)
#         if contract_form.is_valid():
#             contract = contract_form.save()
#             contract.angajat_id = employee_id
#             contract.save()
#         return redirect('/')
#
#     return render(request, 'pages/update_contract.html', {'contract_form': contract_form, 'employee': employee})


# django forms online
# @StaffStatusDecorator
# def status_and_contract_update_view(request, employee_id):
#     employee = Employee.objects.get(id=employee_id)
#     contract = ContractDetail.objects.filter(angajat=employee).last()
#     status = StatusAngajat.objects.filter(angajat=employee).last()
#
#     contract_form = ContractDetailUpdateForm(instance=contract)
#     status_form = StatusAngajatUpdateForm(instance=status)
#
#     if request.method == 'POST':
#         contract_form = ContractDetailUpdateForm(request.POST)
#         status_form = StatusAngajatUpdateForm(request.POST)
#         if contract_form.is_valid() and status_form.is_valid():
#             status = status_form.save()
#             contract = contract_form.save()
#             status.angajat_id = employee_id
#             contract.angajat_id = employee_id
#             status.save()
#             contract.save()
#         return redirect('/')
#
#     return render(request, 'pages/update_contract_status.html', {'contract_form': contract_form,
#                                                                  'employee': employee, 'status_form': status_form})


#
# def hello(request):
#     print(request.GET)
#     print(request.POST)
#     print(request.META)
#     return HttpResponse('Hello, World!')


### functie si probe pentru lista_angajati_cu_ore inainte de a o muta in utils:
# ///////
# for angajat in lista_pontaj:
#     #     ora_lucrata = ore_lucrate_ok(angajat, lista_pontaj)
#     #     print(f"ora lucrata INSIDE FOR:{ora_lucrata}")
#     # print(f"ora lucrata outside for:{ora_lucrata}")
#
#     # for angajat in lista_pontaj:
#     #     ora_lucrata = ore_lucrate_ok(lista_pontaj)
#     #     # print(f"ore lucrate ok TEST: {ore_lucrate_ok(lista_pontaj)}")
#     #     print(f"ora lucrata NOU: {ora_lucrata}")
# //////////
#   ore_lucrate = 0
#
# status_angajat_ok = []
# for angajat in lista_pontaj:
# # la sfarsitul for-ului, vom avea ore lucrate, rotunjite astfel:
# ## - ce are la minute lucrate intre 0 si 14 inclusiv, se scade 0.5 din ore lucrate (ex: 8:14 -> 7.5 ore)
# ## - ce are la minute lucrate intre 46 si 59, se adauga 0.5 la ore lucrate (ex: 8:46 -> 8.5 ore)
# ## - ce e cuprins la minute intre 15 si 45, ramane numarul de ore calculate (ex: 8:40 -> 8 ore)
#     for status_angajat in angajat[1]:
#         # if status_angajat.hours_worked() == 0:
#         #
#         ore = status_angajat.hours_worked()
#         ora = str(ore)
#         try:
#         # if ora[0] == "-":
#         #     ora = "0:00:00"
#         # else:
#             if ":" in ora:
#                 print(f"ora: {ora}")
#                 ora_test = ora.split(":")
#
#                 print(f"ora test:{ora_test}")
#                 print(f"ora 0:{ora_test[0]}")
#                 print(f"int ora 0:{int(ora_test[0])}")
#
#                 print(f"ora 1:{ora_test [1]}")
#                 print(f"ora 2:{ora_test [2]}")
#                 # if int(ora_test[0]) == 0 and int(ora_test[1]) < 30:
#                 #     print(f"merge pe if")
#                 #     ore_lucrate = 0
#
#                 # elif int(ora_test[0]) > 0 and 30 < int(ora_test[1]) < 45:
#                 # if int(ora_test[0]) > 0 and (30 < int(ora_test[1]) < 45 or 0 < int(ora_test[1]) < 15):
#                 if int(ora_test[0]) > 0 and 0 <= int(ora_test[1]) < 15:
#                     print("merge pe if")
#                     ore_lucrate = int(ora_test[0]) - 0.5
#
#                     # ora_test_int = int(ora_test[0])
#                     # ore_lucrate = float(ora_test_int - 0.5)
#                 elif int(ora_test[0]) > 0 and 46 <= int(ora_test[1]) <= 59:
#                     print("merge pe elif")
#                     ore_lucrate = int(ora_test[0]) + 0.5
#                     # status_angajat_ok = [status_angajat, ore_lucrate]
#                     # print(f"elem list pont: {status_angajat_ok}")
#                 else:
#                     print(f"else: int ora test 0: {int(ora_test[0])}")
#                     ore_lucrate = int(ora_test[0])
#                 print(f"ore lucrate: {ore_lucrate}")
#                 # return ore_lucrate
#
#
#
#
#             print(f"status angajat .hours_worked(): {status_angajat.hours_worked()}")
#             print(f"status angajat: {status_angajat}")
#         except ValueError:
#             print('except valuer error raised')
#             ore_lucrate = 999

# //////////////////////////
#     # for element_lista_pontaj in angajat[1]:
#     #     element_lista_pontaj = (element_lista_pontaj, ore_lucrate)
#     #     status_angajat_ok = [status_angajat, ore_lucrate]
#     #     print(f"elem list pont: {status_angajat_ok}")
#
#
#         status_angajat_ok.append([status_angajat, ore_lucrate])
#     print(f"angajat[1]: {angajat[1]}")
#     print(f"lista dupa angajat[1]: {lista_pontaj}")
#     print(f"status angajat ok: {status_angajat_ok}")


#
# def create_year_calendar(request):
#     """
#     Generates in the database the calendar for a hole year with legal free days and special mark for week end days.
#     """
#     if request.method == 'POST':
#         year_ = int(request.POST.get('create_year_value', timezone.now().year))  # ????? ce pun ca si alternativa?
#         sdate = datetime.date(year_, 1, 1)
#         edate = datetime.date(year_, 12, 31)
#         year_days = [sdate + datetime.timedelta(days=x) for x in range((edate - sdate).days)]
#         year_days.append(edate)
#         for day_ in year_days:
#             if day_.weekday() in (5, 6):
#                 GeneratedCalendar.objects.create(date=day_, weekend=True)
#             else:
#                 GeneratedCalendar.objects.create(date=day_)
#     calendar_days = GeneratedCalendar.objects.all()
#     all_days = []
#     for day_ in calendar_days:
#         all_days.append(
#             {'c_year': day_.date.year, 'c_month': day_.date.month, 'c_day': day_.date.day,
#              'c_weekday': day_.date.weekday(), 'free_day': day_.free_day, 'week_end_day': day_.weekend})
#     return render(request, 'calendar_create.html', {'all_days': all_days})
#
# def pontaj_list(request):
#     table = PontajTable(StatusAngajat.objects.all())
#
#     return render(request, 'pages/pontaj_list.html', {'table': table}
#
# def hompage_vechi_view(request): #nu e in urls
#     if request.method == 'POST':
#         angajat_id = request.POST.get('angajat_id')
#         activate = request.POST.get('activate')
#         employee = Employee.objects.get(id=angajat_id)
#
#         user = employee.user
#         user.is_active = True
#         user.save()
#
#     toti_angajatii = Employee.objects.all()
#     date_companii = Company.objects.all()
#     angajati_cu_detalii = []
#     for angajat in toti_angajatii:
#         last_status = StatusAngajat.objects.filter(angajat=angajat).order_by('id').last()
#         actual_contract_details = ContractDetail.objects.filter(angajat=angajat).last()
#         # last_id_contract = actual_contract_details.objects.get(id=actual_contract_details.id)
#         last_comment = Comment.objects.filter(angajat=angajat).last()
#         angajati_cu_detalii.append({'toti_ang': toti_angajatii, 'angajat': angajat, 'last_status': last_status,
#                                     'actual_contract_details': actual_contract_details, 'last_comment': last_comment})
#     table_head = ['NUME', 'PRENUME', 'STARE', 'FUNCTIE', 'SALAR BRUT', 'NAVETA', 'COMENTARII', 'USER']
#     contract_details = None
#     if request.user.is_authenticated:
#         angajat = Employee.objects.get(user_id=request.user.id)
#         contract_details = ContractDetail.objects.filter(angajat=angajat).first()
#     else:
#         angajat = None
#     return render(request, 'pages/homepage.html',
#                   {'angajati_cu_detalii': angajati_cu_detalii, 'companii': date_companii, 'angajat': angajat,
#                    'head_table': table_head, 'contract_details': contract_details})

# class CommentDetailView(PermissionRequiredMixin, StaffRequiredMixin, DetailView):
#     permission_required = 'angajati.view_comment'
#     model = Comment
#     form_class = CommentFormAngajat
#     template_name = ''
#     success_url = '/'

#
# # TODO sa fac crearea pe grupuri/ tipuri de useri, momentan anulat view-ul de mai jos
# @StaffStatusDecorator
# def angajat_create_and_register_view(request):
#     if request.method == 'POST':
#         angajat_form = AngajatForm(data=request.POST)
#         user_form = UserCreationForm(data=request.POST)
#         if angajat_form.is_valid() and user_form.is_valid():
#             angajat: Employee = angajat_form.save()
#             user = user_form.save()
#             # salvam profilul de angajat si facem un update la coloana user si legam de user-ul creat mai sus
#             angajat.user = user
#             angajat.save()
#             if '_create' in request.POST:
#                 return redirect('/')
#             elif '_continue' in request.POST:
#                 return redirect(f"/create_contract_angajat/{angajat.id}/")
#             else:
#                 return redirect(f"/creare_angajat/")
#
#     else:  # method=get
#         # instantiem 2 formulare goale si le trimitem catre template a1b2c3d4.
#         angajat_form = AngajatForm()
#         user_form = UserCreationForm()
#
#     # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
#     return render(request, 'nefolosite/create_and_register_angajat.html',
#                   {'angajat_form': angajat_form, 'user_form': user_form})


# INFO: logica upload_pontaj - incarca fisierul din programul de pontaj si creeaza status_angajat in baza id_pontaj
# pt fiecare angajat din baza de date la care se regaseste id_pontaj din fisierul de upload/ de pontaj
# @StaffStatusDecorator
# def upload_pontaj_csv_view(request):
#     print(request.GET)
#     print(request.POST)
#     print(request.FILES)
#     try:
#         if request.method == 'POST' and request.FILES['csv_file']:
#             csv_file = request.FILES['csv_file']  # l-am citit din dictionar
#             fs = FileSystemStorage()
#             filename = fs.save(csv_file.name, csv_file)
#             # uploaded_file_url = fs.url(filename)
#             print(filename)
#             with open(filename) as csv_file:
#                 csv_reader = csv.reader(csv_file)
#                 line_count = 0
#                 for row in csv_reader:
#                     line_count += 1
#                     if line_count < 17:
#                         continue
#
#                     row_type = row[0]
#                     if row_type == "0":
#                         employee_details = row[1]
#                         id_start_index = employee_details.find('ID: ') + len("ID: ")
#                         id_stop_index = employee_details.find(')', id_start_index)
#                         id_pontaj_angajat = employee_details[id_start_index:id_stop_index]
#                         angajat = Employee.objects.filter(id_pontaj=id_pontaj_angajat).first()
#
#                     elif row_type == "1":
#                         _, date, hours_worked, first_in, last_out = row
#                         # print(f"date: {date}")
#
#                         if " - " in date:  # cazul in care avem 2 date, intrat azi, iesit maine:
#                             dates = date.split(" - ")
#                             date1 = dates[0]
#                             # print(f"dates 0{dates[0]}")
#                             # print(f"dates 1{dates[1]}")
#                             # date1 = utils.date_ro_to_date_int(dates[0])
#                             # date2 = utils.date_ro_to_date_int(dates[1])
#                             date_time_in = datetime.strptime(f"{dates[0]} {first_in}", '%Y/%m/%d %H:%M:%S')
#                             date_time_out = datetime.strptime(f"{dates[1]} {last_out}", '%Y/%m/%d %H:%M:%S')
#                             # print(f"d t in{date_time_in}")
#                             # print(f"d t out{date_time_out}")
#                         else:  # avem 1 data
#                             try:
#                                 date1 = date
#                                 # date_time_in = datetime.strptime(f"{date} {first_in}", '%d/%m/%Y %H:%M:%S')
#                                 date_time_in = datetime.strptime(f"{date} {first_in}", '%Y/%m/%d %H:%M:%S')
#                                 # date_time_out = datetime.strptime(f"{date} {last_out}", '%d/%m/%Y %H:%M:%S')
#                                 date_time_out = datetime.strptime(f"{date} {last_out}", '%Y/%m/%d %H:%M:%S')
#                             except ValueError:
#                                 date_time_in = datetime.strptime(f"{date} {first_in}", '%d/%m/%Y %H:%M:%S')
#                                 date_time_out = datetime.strptime(f"{date} {last_out}", '%d/%m/%Y %H:%M:%S')
#
#                         # day, month, year = date.split("/")
#                         # new_date = f"{year}-{month}-{day}"
#                         angajat_status = Status.objects.filter(abbreviated_value='ANG').first()
#                         if angajat is None:
#                             continue
#                         existing_date_status = StatusAngajat.objects.filter(angajat=angajat,
#                                                                             date_time_in__year=date_time_in.year,
#                                                                             date_time_in__month=date_time_in.month,
#                                                                             date_time_in__day=date_time_in.day)
#                         if existing_date_status.count() > 0:
#                             continue
#                         status_angajat = StatusAngajat.objects.create(status=angajat_status, angajat=angajat,
#                                                                       date_time_in=date_time_in,
#                                                                       date_time_out=date_time_out)
#                     # print(f"row{row}")
#                 # print(f'Processed {line_count} lines.')
#     except MultiValueDictKeyError:
#         # print("MultiValueDictKeyError: nu s-a selectat niciun fisier pentru incarcare")
#         response = HttpResponse()
#         response.write("<p><br/>Nu s-a selectat niciun fisier pentru incarcare!</p>")
#         response.write("<p><br/>Reveniti la meniul anterior si alegeti prima data un fisier, apoi apasati "
#                        "butonul 'Upload csv for processing'!</p>")
#         return response
#     return render(request, 'pages/upload_pontaj_csv.html', {})
