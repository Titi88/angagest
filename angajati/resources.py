# https://www.codeunderscored.com/how-to-use-django-import-export-library/#Back_to_Django-Import-Export
# sau
# https://django-import-export.readthedocs.io/en/latest/installation.html#import-export-use-transactions

import tablib
from import_export import resources
from import_export.fields import Field
from import_export.widgets import DateWidget

from angajati.models import *


class EmployeeResource(resources.ModelResource):

    class Meta:
        model = Employee
        fields = ('nume_angajat', 'prenume_angajat', 'id_pontaj', 'cnp', 'telefon', 'religie','email', 'id', 'data_angajare', 'data_nasterii')
        # exclude = ('user', 'nr_cartela', 'data_introducere')


class StatusResource(resources.ModelResource):
    class Meta:
        model = Status


# class StatusAngajatResource(resources.ModelResource):
#     def export(self, queryset=None, *args, **kwargs):
#         all_employees = Employee.objects.all()
#         queryset = StatusAngajat.objects.all().get(angajat=all_employees)
#         print(f"queryset: {queryset}")
#     class Meta:
#         model = StatusAngajat
#         ----https://stackoverflow.com/questions/67657448/export-two-querysets-into-one-excel-using-django-import-export
#


