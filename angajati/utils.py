# from workalendar.europe import Romania
from datetime import date, datetime, timedelta
from calendar import *
import calendar
import datetime

from django.core.exceptions import ObjectDoesNotExist

from angajati.models import *

def ore_lucrate_ok(lista_pontaj):
    # primesc o lista cumpuse din liste aferente fiecarui angajat
    ## fiecare lista de angajat din interior are [angajatul, [lista cu toate statusurile]]
    # la sfarsitul for-ului, vom avea ore lucrate, rotunjite astfel:
    ## - ce are la minute lucrate intre 0 si 14 inclusiv, se scade 0.5 din ore lucrate (ex: 8:14 -> 7.5 ore)
    ## - ce are la minute lucrate intre 46 si 59, se adauga 0.5 la ore lucrate (ex: 8:46 -> 8.5 ore)
    ## - ce e cuprins la minute intre 15 si 45, ramane numarul de ore calculate (ex: 8:40 -> 8 ore)
    lista_angajati_cu_ore = []
    # lista_angajat_cu_ore = []
    for angajat in lista_pontaj:
        # la sfarsitul for-ului, vom avea ore lucrate, rotunjite astfel:
        ## - ce are la minute lucrate intre 0 si 14 inclusiv, se scade 0.5 din ore lucrate (ex: 8:14 -> 7.5 ore)
        ## - ce are la minute lucrate intre 46 si 59, se adauga 0.5 la ore lucrate (ex: 8:46 -> 8.5 ore)
        ## - ce e cuprins la minute intre 15 si 45, ramane numarul de ore calculate (ex: 8:40 -> 8 ore)
        lista_angajat_cu_ore = [angajat[0], [], 0]
        for status_angajat in angajat[1]:
            ore = status_angajat.hours_worked()
            ora = str(ore)
            try:
                if ":" in ora:
                    ora_string = ora.split(":")

                    if int(ora_string[0]) > 0 and 0 <= int(ora_string[1]) < 15:
                        ore_lucrate = int(ora_string[0]) - 0.5
                    elif int(ora_string[0]) > 0 and 46 <= int(ora_string[1]) <= 59:
                        ore_lucrate = int(ora_string[0]) + 0.5
                    else:
                        ore_lucrate = int(ora_string[0])
                else:
                    ore_lucrate = 0
            except ValueError:
                ore_lucrate = 999

            lista_angajat_cu_ore[1].append(ore_lucrate)
            # lista_angajat_cu_ore[2] = 5555
            lista_angajat_cu_ore[2] = sum(lista_angajat_cu_ore[1])
        lista_angajati_cu_ore.append(lista_angajat_cu_ore)


    #INFO: returnez o lista de liste :
    ## fiecare lista reprezinta statusul unui angajat si are:
    ### [angajatul, [lista cu ore rotunjite pentru fiecare zi de pontaj], suma orelor rotunjite]
    return lista_angajati_cu_ore


def ore_lucrate_ok_angajat(lista_pontaj):
    # primesc o lista cumpusa din [angajat, [lista cu toate statusurile]]
        # la sfarsitul for-ului, vom avea ore lucrate, rotunjite astfel:
        ## - ce are la minute lucrate intre 0 si 14 inclusiv, se scade 0.5 din ore lucrate (ex: 8:14 -> 7.5 ore)
        ## - ce are la minute lucrate intre 46 si 59, se adauga 0.5 la ore lucrate (ex: 8:46 -> 8.5 ore)
        ## - ce e cuprins la minute intre 15 si 45, ramane numarul de ore calculate (ex: 8:40 -> 8 ore)
    lista_angajat_cu_ore = [lista_pontaj[0], [], 0]
    for status_angajat in lista_pontaj[1]:
        ore = status_angajat.hours_worked()
        ora = str(ore)
        try:
            if ":" in ora:
                ora_string = ora.split(":")
                if int(ora_string[0]) > 0 and 0 <= int(ora_string[1]) < 15:
                    ore_lucrate = int(ora_string[0]) - 0.5
                elif int(ora_string[0]) > 0 and 46 <= int(ora_string[1]) <= 59:
                    ore_lucrate = int(ora_string[0]) + 0.5
                else:
                    ore_lucrate = int(ora_string[0])
            else:
                ore_lucrate = 0
        except ValueError:
            ore_lucrate = 999

        lista_angajat_cu_ore[1].append(ore_lucrate)
        # lista_angajat_cu_ore[2] = 5555
        lista_angajat_cu_ore[2] = sum(lista_angajat_cu_ore[1])


    # returnez o lista de liste :
    ## fiecare lista reprezinta statusul angajatului si are:
    ### [angajatul, [lista cu ore rotunjite pentru fiecare zi de pontajul selectat], suma orelor rotunjite]
    return lista_angajat_cu_ore


def generate_month_calendar(year_, month_):

    month_name = calendar.month_name[month_]
    week_days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
    c = calendar.Calendar()

    list_month = list(c.itermonthdays(year_, month_))
    day_idx = 0
    week_dict = {}
    month_list = []

    for day_ in list_month :
        if day_ != 0 and day_idx < 31:
            if day_idx in (5, 6):
                week_dict[week_days[day_idx]] = ['btn-danger', day_, 'week end']
            else:
                week_dict[week_days[day_idx]] = ['btn-secondary', day_, 'week day']
        else:
            week_dict[week_days[day_idx]] = ['btn-secondary', '', '']

        print(f"week dict: {week_dict}")

        day_idx += 1
        if day_idx == 7:
            month_list.append(week_dict)
            week_dict = {}
            day_idx = 0

    print(f"week dict: {week_dict}")
    return list_month, month_list, month_name, week_days



# def generate_monthly_calendar(year_, month_):
#     month_name = calendar.month_name[month_]
#     week_days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
#     c = calendar.Calendar()
#
#     list_month = list(c.itermonthdays(year_, month_))
#     print(f"list month: {list_month}")
#
#     pass
#
# if __name__ == '__main__':
#

# class Calendar(HTMLCalendar):
#     def __init__(self, year=None, month=None):
#         self.year = year
#         self.month = month
#         super(Calendar, self).__init__()





    #     #     session = StatusAngajat.objects.filter(date_time_in=date_).all()
    #     #     if session :
    #     day_idx += 1
    #     if day_idx == 7 :
    #         month_list.append(week_dict)
    #         week_dict = {}
    #         day_idx = 0

    # return month_list, month_name, week_days

#
#
# def date_ro_to_date_int(ro_date_string):
#     day, month, year = ro_date_string.split("/")
#     return f"{year}-{month}-{day}"

# if __name__ == '__main__':
#     print(generate_month_calendar(2021, 12))



# https://docs.python.org/3/library/calendar.html
#

#
#     # formats a day as a td
#     # filter events by day
#     def formatday(self, day, statuses):
#         statuses_per_day = statuses.filter(start_time__day=day)
#         d = ''
#         for stat in statuses_per_day:
#             d += f'<li> {stat.status} </li>'
#
#         if day != 0:
#             return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
#         return '<td></td>'
#
#     # formats a week as a tr
#     # def formatweek(self, theweek, events):
#     def formatweek(self, theweek):
#         week = ''
#         for d, weekday in theweek:
#             # week += self.formatday(d, events)
#             week += self.formatday(d)
#         return f'<tr> {week} </tr>'
#
#     # formats a month as a table
#     # filter events by year and month
#     def formatmonth(self, theyear, themonth, withyear=True):
#         statuses = StatusAngajat.objects.filter(date_time_in__year=self.year, date_time_out__month=self.month)
#
#         cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
#         cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
#         cal += f'{self.formatweekheader()}\n'
#         for week in self.monthdays2calendar(self.year, self.month):
#             cal += f'{self.formatweek(statuses)}\n'
#         return cal

# if __name__ == '__main__':
#     cal = Romania()
#     print(cal.holidays(2021))

# >>> cal.is_working_day(date(2012, 12, 25))  # it's Christmas
# False
# >>> cal.is_working_day(date(2012, 12, 30))  # it's Sunday
# False
# >>> cal.is_working_day(date(2012, 12, 26))
# True
# >>> cal.add_working_days(date(2012, 12, 23), 5)  # 5 working days after Xmas
# datetime.date(2012, 12, 31)
