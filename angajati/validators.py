from functools import reduce
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _



def validare_cnp(value):
    CHEIE_TESTARE = "279146358279"
    if value[0] != "0":
        rezultat_testare = reduce(lambda x, y: x + y, list(map(lambda x, y: int(x) * int(y), value, CHEIE_TESTARE)))
        a = rezultat_testare % 11
        if a < 10 and a == int(value[-1]):
            pass
        elif a == 10 and int(value[-1]) == 1:
            pass
        else:
            raise ValidationError(_(f'CNP:{value} is not valid'),)
    else:
        raise ValidationError(_(f'CNP:{value} cannot start with 0'),)
